use std::io::{BufRead, stdin};

use tofex::{Item, Tofex};



fn main() -> Result<(), String> {
    let stdin = stdin();
    let mut handle = stdin.lock();

    let mut choices: Vec<String> = Vec::new();
    let mut buff = String::new();
    loop {
        if 0 == handle.read_line(&mut buff)
            .map_err(|e| format!("error reading from stdin: {}", &e))?
        {
            break;
        }
        let mut choice = String::new();
        std::mem::swap(&mut buff, &mut choice);
        choices.push(choice);
    }

    let items: Vec<&str> = choices.iter().map(|s| s.as_str()).collect();
    let tofex = Tofex::default().with_config("example.conf");
    let choice = tofex.select("choose:", &items);

    println!("{:?}", &choice);
    Ok(())
}
