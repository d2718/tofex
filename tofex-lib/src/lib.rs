/*!
`tofex` is a library to use
[`tofi`](https://github.com/philj56/tofi)
as option picker, while adding a little richness to its spartan,
`dmenu`-like operation.

```
use tofex::Tofex;

static REGRESSIONS: &[(&str, &str)] = &[
    ("line, "Linear"),
    ("p2", "Quadratic"),
    ("p3", "Cubic"),
    ("ln", "Logarithmic"),
    ("log", "Logistic"),
    ("sin", "Sinusoidal"),
];

fn main() {
    let t = Tofex::default();

    match t.select("regression:", REGRESSIONS).unwrap() {
        Some(n) => println!(
            "You selected to use a {} regression",
            REGRESSIONS[n].1
        ),
        None => println!("You declined to choose.")
    }
}
```


*/

use std::io::{BufWriter, Read, Write};
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};

const NEWLINE: u8 = b'\n';
const BUFF_SIZE: usize = 256;

/**
Implement this trait for any type you want to use as a `tofi` selector.

The original use case for this whole idea was as a wrapper around
[`dmenu`](https://tools.suckless.org/dmenu/) to allow more descriptive
options than just the text of each choice. (I called this original version
"fat dmenu".) Part of the idea was that each option would have a "verbose"
description, but then also start with a short, easy-to-type "key" for fast
selection with the keyboard.

So wrapping `tofi` this way to implement a program launcher might look like

```text
ff       Firefox Web Browser
geany    Geany Text Editor
pragha   Pragha Music Player
soffice  LibreOffice Suite
term     Alacritty Terminal Emulator
vlc      VLC Media Player
wx       Current Local Weather
```

The `[Item::key_len]` function is meant to return the length of the "key"
string, so that the length of the longest key can be passed to `[Item::line]`
(as the `max_key_len` argument), so it can format its line such that all the
"verbose" elements line up, as above.

See the implementation of `Item` for two-tuples of `AsRef<str>` for a
concrete example that may be explanatory.
*/
pub trait Item {
    /**
    Return the length of this `Item`'s "key". If your type's formatting
    scheme doesn't have a "key" portion or care about its length, then this
    function's return value doesn't matter.
    */
    fn key_len(&self) -> usize;

    /**
    Format the text of this `Item`'s option line as it should be displayed
    in `tofi`. `[Tofex::select]` will call `[Item::key_len]` on each item
    in the slice of `Item`s passed to it; the largest of these will then be
    passed as the `max_key_len` argument when it calls `[Item::line]`.
    */
    fn line(&self, max_key_len: usize) -> Vec<u8>;
}

impl<T, U> Item for (T, U)
where
    T: AsRef<str>,
    U: AsRef<str>
{
    fn key_len(&self) -> usize {
        self.0.as_ref().chars().count()
    }

    fn line(&self, max_key_len: usize) -> Vec<u8> {
        format!(
            "{:kwidth$}  {}\n",
            &self.0.as_ref(),
            &self.1.as_ref(),
            kwidth = max_key_len
        ).into_bytes()
    }
}

impl Item for &str {
    fn key_len(&self) -> usize { 0 }

    fn line(&self, _: usize) -> Vec<u8> {
        let mut v: Vec<u8> = Vec::with_capacity(self.len()+2);
        v.extend_from_slice(self.as_bytes());
        v
    }
}

pub struct Tofex {
    pub tofi: PathBuf,
    pub cfg_file: Option<PathBuf>,
}

impl Tofex {
    fn cmd(&self, prompt: &str) -> Command {
        let mut cmd = Command::new(&self.tofi);
        if let Some(p) = self.cfg_file.as_ref() {
            cmd.arg("--config").arg(p);
        }
        cmd.args(["--prompt-text", prompt]);
        cmd.stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::inherit());

        cmd
    }

    pub fn new() -> Tofex {
        Tofex { tofi: PathBuf::from("tofi"), cfg_file: None }
    }

    pub fn with_config<P: AsRef<Path>>(self, p: P) -> Tofex {
        let mut tofi = self;
        tofi.cfg_file = Some(PathBuf::from(p.as_ref()));
        tofi
    }

    pub fn select<S, I>(&self, prompt: S, items: &[I]) -> Result<Option<usize>, String>
    where
        S: AsRef<str>,
        I: Item
    {
        let klen: usize = items.iter().map(|itm| itm.key_len()).max().unwrap_or(0);

        let choices: Vec<Vec<u8>> = items.iter().map(|item| {
            let mut v = item.line(klen);
            if Some(&NEWLINE) == v.last() {
                v
            } else {
                v.push(NEWLINE);
                v
            }
        }).collect();

        let mut child = self.cmd(prompt.as_ref())
            .spawn()
            .map_err(|e| format!("unable to launch tofi: {}", &e))?;
        {
            let mut stdin = BufWriter::new(child.stdin.take().unwrap());
            for line in choices.iter() {
                stdin.write_all(line.as_slice()).map_err(|e| format!("error writing to tofi subprocess: {}", &e))?;
                
            }

            stdin.flush().map_err(|e| format!("error writing to tofi subprocess: {}", &e))?;
        }
        
        let mut stdout = child.stdout.take().unwrap();
        child.wait().map_err(|e| format!("tofi subprocess returned error: {}", &e))?;
        let mut choice_bytes: Vec<u8> = Vec::with_capacity(BUFF_SIZE);
        stdout.read_to_end(&mut choice_bytes).map_err(|e| format!("error reading tofi output: {}", &e))?;

        for (n, line) in choices.iter().enumerate() {
            // eprintln!(
            //     "{:?} vs {:?}",
            //     String::from_utf8_lossy(&choice_bytes),
            //     String::from_utf8_lossy(line)
            // );
            if *line == choice_bytes {
                return Ok(Some(n));
            }
        }
        
        Ok(None)
    }
}

fn get_default_config() -> Option<PathBuf> {
    let home = std::env::var("HOME").ok()?;
    let mut home = PathBuf::from(home);
    home.push(".config");
    home.push("tofex.conf");
    Some(home)
}

impl Default for Tofex {
    fn default() -> Self {

        Tofex { tofi: "tofi".into(), cfg_file: get_default_config() }
    }
}
