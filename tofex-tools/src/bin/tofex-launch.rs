use std::io::BufReader;
use std::fs::File;
use std::path::Path;

use eyre::{WrapErr, OptionExt};
use serde::Deserialize;

use tofex::{Item, Tofex};

#[derive(Debug, Deserialize)]
struct Exec {
    key: String,
    desc: String,
    exec: Vec<String>,
}

#[derive(Debug, Deserialize)]
struct Menu {
    key: String,
    desc: String,
    entries: Vec<Entry>,
}

#[derive(Deserialize, Debug)]
#[serde(untagged)]
enum Entry {
    Exec(Exec),
    Menu(Menu),
}

impl Item for Entry {
    fn key_len(&self) -> usize {
        match self {
            Entry::Exec(x) => x.key.chars().count(),
            Entry::Menu(m) => m.key.chars().count(),
        }
    }

    fn line(&self, max_key_len: usize) -> Vec<u8> {
        match self {
            Entry::Exec(x) => format!(
                "{:kwid$}   {}\n",
                &x.key, &x.desc,
                kwid = max_key_len
            ).into_bytes(),
            Entry::Menu(m) => format!(
                "{:kwid$}/  {}\n",
                &m.key, &m.desc,
                kwid = max_key_len
            ).into_bytes(),
        }
    }
}

fn load_menu<P: AsRef<Path>>(path: P) -> eyre::Result<Vec<Entry>> {
    let path = path.as_ref();
    let f = File::open(path).wrap_err("opening menu file")?;
    let mut bufreader = BufReader::new(f);
    let entries: Vec<Entry> = serde_json::from_reader(&mut bufreader)
        .wrap_err("reading from menu file")?;
    Ok(entries)
}

fn exec<S: AsRef<str>>(chunks: &[S]) -> ! {
    use std::ffi::CString;
    use std::os::raw::c_char;

    // Turn the command and arguments int a Vec of C-style strings.
    let args: Vec<CString> = chunks.iter()
        .map(|chunk| CString::new(chunk.as_ref().as_bytes()).unwrap())
        .collect();

    // Create a Vec of pointers to our C-style strings;
    let mut arg_ptrs: Vec<*const c_char> = args.iter().map(|a| a.as_ptr()).collect();

    // Terminate our Vec of pointers with a null pointer; that's how C
    // detects the end of the array.
    arg_ptrs.push(std::ptr::null());
    // The pointer to our Vec of pointers. This is real-Cy now.
    let argv: *const *const c_char = arg_ptrs.as_ptr();

    // `execvp()` expects its second argument to be our "argument vector"
    // (null-terminated array of null-terminated arrays of char); it also
    // expects the _first_ argument to be the name of the target program,
    // which should be the first element of our argument vector. Yeah, that
    // first argument ends up getting passed twice. Obviously, if you screw
    // this up, you'll get segfaults.
    let res = unsafe { libc::execvp(arg_ptrs[0], argv) };

    // If we're `exec`ing, we definitely shouldn't return, so if we're still
    // here, we're going to panic about it.
    if res < 0 {
        panic!("error executing: {}", &res);
    } else {
        panic!("execvp() returned for some reason. This def shouldn't happen.");
    }
}

fn recursive_select<'a>(prompt: &str, items: &'a [Entry]) -> Option<&'a Exec> {
    let tofex = Tofex::default();

    loop {
        match tofex.select(prompt, items).unwrap()
        {
            None => return None,
            Some(n) => match &items[n] {
                Entry::Exec(x) => return Some(x),
                Entry::Menu(m) => {
                    let new_prompt = format!("{}{}/", prompt, &m.key);
                    if let Some(x) = recursive_select(&new_prompt, &m.entries) {
                        return Some(x);
                    }
                }
            }
        }
    }
    
}

fn main() -> eyre::Result<()> {
    let menu_file = std::env::args().nth(1).ok_or_eyre("must specify menu file")?;

    let entries = load_menu(&menu_file).wrap_err("failed to load menu file")?;

    if let Some(x) = recursive_select("/", &entries) {
        exec(&x.exec);
    }

    Ok(())
}
